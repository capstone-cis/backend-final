package com.sd3.capstone.dtos;

public record UserDTO(String name, String login, String password) {

}