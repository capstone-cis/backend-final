package com.sd3.capstone.controllers;

import com.sd3.capstone.dtos.UserDTO;
import com.sd3.capstone.entities.User;
import com.sd3.capstone.Services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserControllers {
    @Autowired
    private UserServices userService;

    @PostMapping("/create")
    public ResponseEntity<User> saveUser(@RequestBody UserDTO userDTO){
        var user = userService.saveUser(userDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }


    @GetMapping
    public ResponseEntity<List<User>> getUser(){
        return ResponseEntity.ok(userService.getUser());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable String id){
        userService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable String id, @RequestBody UserDTO userDTO){
        var user = userService.updateUser(id,userDTO);
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }

    @GetMapping("/{login}")
    public ResponseEntity <User> getUserByLogin(@PathVariable String login){
        return ResponseEntity.ok(userService.getUserByLogin(login));
    }
}
