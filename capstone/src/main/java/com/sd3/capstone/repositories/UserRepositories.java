package com.sd3.capstone.repositories;

import com.sd3.capstone.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface UserRepositories extends MongoRepository <User, String> {
    Optional <User> findByLogin(String login);

}

