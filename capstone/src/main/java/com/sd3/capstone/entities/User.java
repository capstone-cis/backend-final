package com.sd3.capstone.entities;

import com.sd3.capstone.dtos.UserDTO;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document("users")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class User {

    @Field(name = "_id") @MongoId
    private String id;
    @Field(name = "login")
    private String login;
    @Field(name = "name")
    private String name;
    @Field(name = "password")
    private String password;

    public User(UserDTO userDTO){
        this.name = userDTO.name();
        this.login = userDTO.login();
        this.password = userDTO.password();
    }

}
